#!/bin/sh

basedir=`pwd`

cd "$basedir/base"

git reset --hard origin/master

git am --abort
git am -3 "$basedir/patches/"*.patch

if [ "$?" != "0" ]; then
	echo "  Something did not apply cleanly"
else
	echo "  Patches applied cleanly"
fi
