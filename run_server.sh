#!/bin/sh

basedir=`pwd`

./apply_patches.sh

rsync -a base/ server

cd "$basedir/server"

java -Xmx4G -jar ftb-server.jar
